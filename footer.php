       <div class="row">

       <div class="medium-4 colums">
       <!-- Social media icons, go at end of page -->
       <div class="social-media">

       <a href="https://twitter.com/@band" class="icon-button twitter">
         <i class="fi-social-twitter"></i><span></span>
        </a>
        
        <a href="https://facebook.com/@band" class="icon-button facebook">
          <i class="fi-social-facebook"></i><span></span>
        </a>
        
        <a href="https://instagram.com/band" class="icon-button instagram">
          <i class="fi-social-instagram"></i><span></span>
        </a>

        <a href="https://snapchat.com/add/band" class="icon-button snapchat">
          <i class="fi-social-snapchat"></i><span></span>
        </a>

       </div> <!-- end social media -->
       </div> <!-- end medium-2 columns -->

       </div> <!-- end row -->

    <!-- html5shiv, javascript, from cdn -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js" integrity="sha256-sqQlcOZwgKkBRRn5WvShSsuopOdq9c3U+StqgPiFhHQ=" crossorigin="anonymous"></script>

    <!-- jquery, from cdn -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js" integrity="sha256-ImQvICV38LovIsvla2zykaCTdEh1Z801Y+DSop91wMU=" crossorigin="anonymous"></script>
    <!-- localcopy of jquery, if cdn doesn't load for some reason -->
    <script>window.jQuery || document.write('<script src="/js/jquery-2.1.4.js"><\/script>')</script>

    <!-- foundation javascript, from cdn -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/js/foundation.js" integrity="sha256-qCcAVdFzMTWznf3J5aCBj6yt4+fSHUcXsIi0X9TD0D4=" crossorigin="anonymous"></script>
    <!-- Foundation JS local fallback -->
    <script>window.Foundation || document.write('<script src="/js/foundation.js"><\/script>')</script>
    <!-- Initialize Foundation -->
    <script>$(document).foundation();</script>
    <!-- Foundation CSS local fallback -->
    <script>
     $(document).ready(function() 
     {
       var bodyColor = $('body').css('color');
       if(bodyColor != 'rgb(34, 34, 34)') 
      {
       $("head").prepend('<link rel="stylesheet" href="/css/foundation.min.css">');}});
  </script>