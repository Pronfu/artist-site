<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Events | Example Artist Site</title>

    <!-- Foundation css, from cdn -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/css/foundation.min.css" integrity="sha256-itWEYdFWzZPBG78bJOOiQIn06QCgN/F0wMDcC4nOhxY=" crossorigin="anonymous" />

    <!-- Event list styling - http://zurb.com/building-blocks/event-listing -->
    <link rel="stylesheet" href="css/event.css" /> <!-- styles for event list -->

    <!-- Social media icons - http://zurb.com/playground/foundation-icon-fonts-3 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css" integrity="sha256-CWltC/W+elkkUKhitcztPiSfE3AEpzAvrkmEqB68Lx0=" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/sm-icons.css" />

    <!-- Normalize.css, makes all browsers render everything consistently and in line with modern standards -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.css" integrity="sha256-G5VA29cSH+JxK8+MeAVL36QhUlTv7Fipck5dnVXpUXw=" crossorigin="anonymous" />
    <!-- normalize.css fallback -->
    <script>
    if ($('body').css('margin-left') != 0) 
    {
      $('head').append("/css/normalize-6.0.0.css", "CUSTOM CSS");
    }
    </script>

   <!-- For CSS3 Media Queries on Internet Explorer, https://github.com/scottjehl/Respond -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>

  </head>
  <body>
    

    <?php include('menu.php') ?>

    <br>

    <div class="row">

       <div class="medium-12 columns">

        <p> Please be patient while our events load from our Facebook page. </p>
        
        <div class='dsm-fb-event' embed-id='483'></div><script src='https://www.displaysocialmedia.com/app/embed/facebook-events/widget.js'></script>

       </div> <!-- end column row -->

       <?php include('footer.php'); ?>

  </body>
</html>


    