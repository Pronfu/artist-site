<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Store | Example Artist Site</title>

    <!-- Foundation css, from cdn -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/css/foundation.min.css" integrity="sha256-itWEYdFWzZPBG78bJOOiQIn06QCgN/F0wMDcC4nOhxY=" crossorigin="anonymous" />

    <!-- Social media icons - http://zurb.com/playground/foundation-icon-fonts-3 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css" integrity="sha256-CWltC/W+elkkUKhitcztPiSfE3AEpzAvrkmEqB68Lx0=" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/sm-icons.css" />

    <!-- Normalize.css, makes all browsers render everything consistently and in line with modern standards -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.css" integrity="sha256-G5VA29cSH+JxK8+MeAVL36QhUlTv7Fipck5dnVXpUXw=" crossorigin="anonymous" />
    <!-- normalize.css fallback -->
    <script>
    if ($('body').css('margin-left') != 0) 
    {
      $('head').append("/css/normalize-6.0.0.css", "CUSTOM CSS");
    }
    </script>

   <!-- For CSS3 Media Queries on Internet Explorer, https://github.com/scottjehl/Respond -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>

  </head>
  <body>
    

    <?php include('menu.php') ?>

    <br>

    <div class="row">
      <div class="medium-6 columns"> <!-- main artist image -->
        <img class="thumbnail" src="https://unsplash.it/500/?random">
      </div> <!-- end main artist image -->

      <div class="medium-6 large-5 columns">
        <h3>Featured Image Name</h3>
        <p>This is your featured image. Put a description, reason why people should buy it and a discount.
        <br> <br>
        <a href="#" class="button">Buy This</a></p>
        </div>
    </div>

    <div class="column row">
      <hr>

      <div class="medium-3 columns">
      
      <a href="#"><img src="https://unsplash.it/400/?random" /></a>
      <br> <br>
      <a href="#" class="button">Buy This</a></p>

      </div> <!-- end medium-3 columns -->

      <div class="medium-3 columns">
      
      <a href="#"><img src="http://loremflickr.com/400/250" /></a>
      <br> <br>
      <a href="#" class="button">Buy This</a></p>

      </div> <!-- end medium-3 columns -->

      <div class="medium-3 columns">
      
      <a href="#"><img src="http://lorempixel.com/400/250/" /></a>
      <br> <br>
      <a href="#" class="button">Buy This</a></p>

      </div> <!-- end medium-3 columns -->

      <div class="medium-3 columns">
      
      <a href="#"><img src="http://placebeard.it/400/notag" /></a>
      <br> <br>
      <a href="#" class="button">Buy This</a></p>

      </div> <!-- end medium-3 columns -->

       </div> <!-- end column row -->

       <?php include('footer.php'); ?>

  </body>
</html>


    