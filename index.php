<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Example Artist Site</title>

    <!-- Foundation css, from cdn -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/css/foundation.min.css" integrity="sha256-itWEYdFWzZPBG78bJOOiQIn06QCgN/F0wMDcC4nOhxY=" crossorigin="anonymous" />

    <!-- Event list styling - http://zurb.com/building-blocks/event-listing -->
    <link rel="stylesheet" href="css/event.css" /> <!-- styles for event list -->

    <!-- Social media icons - http://zurb.com/playground/foundation-icon-fonts-3 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css" integrity="sha256-CWltC/W+elkkUKhitcztPiSfE3AEpzAvrkmEqB68Lx0=" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/sm-icons.css" />

    <!-- Normalize.css, makes all browsers render everything consistently and in line with modern standards -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.css" integrity="sha256-G5VA29cSH+JxK8+MeAVL36QhUlTv7Fipck5dnVXpUXw=" crossorigin="anonymous" />
    <!-- normalize.css fallback -->
    <script>
    if ($('body').css('margin-left') != 0) 
    {
      $('head').append("/css/normalize-6.0.0.css", "CUSTOM CSS");
    }
    </script>

   <!-- For CSS3 Media Queries on Internet Explorer, https://github.com/scottjehl/Respond -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>

  </head>
  <body>
    

    <?php include('menu.php') ?>

    <br>

    <div class="row">
      <div class="medium-6 columns"> <!-- main artist image -->
        <img class="thumbnail" src="img/band-photo.jpeg">
      </div> <!-- end main artist image -->

      <div class="medium-6 large-5 columns">
        <h3>Artist Name</h3>
        <p>Fill in a brief description of the artist / band. More details coming soon.... Nunc eu ullamcorper orci. Quisque eget odio ac lectus vestibulum faucibus eget in metus. In pellentesque faucibus vestibulum. Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in.</p>
        </div>
    </div>

    <div class="column row">
      <hr>

       <div class="medium-4 columns">
         <iframe src="https://embed.spotify.com/?uri=spotify%3Auser%3Aprxmusic%3Aplaylist%3A3VEFRGe3APwGRl4eTpMS4x" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>
       </div> <!-- end medium-4 columns -->

       <div class="medium-8 columns">

        <article class="event">
         <div class="event-date">
          <p class="event-month">April</p>
          <p class="event-day">4</p>
          </div> <!-- end event date -->
          
          <div class="event-desc">
           <h4 class="event-desc-header">Worship Night in America / San Antonio, TX </h4>
           <p class="event-desc-detail"><span class="event-desc-time"></span>Freeman Coliseum, San Antonio, TX, United States</p>
           <a href="http://www1.ticketmaster.com/chris-tomlin-san-antonio-texas-04-04-2017/event/3A00516BFFB45EC4?artistid=982298&majorcatid=10001&minorcatid=50&clickid=zWwXEwz4y1yxRyMSz-w8uSoVUkkToHRweUjv0k0&camefrom=CFC_BUYAT_219208&impradid=219208&REFERRAL_ID=tmfeedbuyat219208&wt.mc_id=aff_BUYAT_219208&utm_source=219208-Bandsintown&impradname=Bandsintown&utm_medium=affiliate&irgwc=1" class="rsvp button">Get Tickets</a>
          </div> <!-- end event desc -->
          
        </article> <!-- end event -->
        
        <hr>

        <article class="event">
         <div class="event-date">
          <p class="event-month">May</p>
          <p class="event-day">13</p>
          </div> <!-- end event date -->
          
          <div class="event-desc">
           <h4 class="event-desc-header">Worship Night in America / Rio Rancho, NM </h4>
           <p class="event-desc-detail"><span class="event-desc-time"></span>Santa Ana Star Center, Rio Rancho, NM, United States</p>
           <a href="http://l.facebook.com/l.php?u=http%3A%2F%2Fev9.evenue.net%2Fcgi-bin%2Fncommerce3%2FSEGetEventInfo%3FticketCode%3DGS%253ARIO%253ASASC17%253ACT0513%253A%26linkID%3Dglobal-rio%26shopperContext%26pc%26caller%26appCode%26groupCode%3DCHANCE%26cgc&h=ATNKyo4-zDwFHZKMEdzTG0L84u-Z1T1o8ic_yL3NDJUedJ0gbEt_BsbCK4zQBAuTQF9WmQiiWRPbw-2uLtWaUpbZU-5ndEex1tN_JeMn-JB25nDv5JQu06t-g66ShJlxoYwyMdk" class="rsvp button">Get Tickets</a>
          </div> <!-- end event desc -->
          
        </article> <!-- end event -->

       </div> <!-- end events -->

       <h3> Find all our events on our <a href="events">events page</a>. </h3>

       </div> <!-- end column row -->

       <?php include('footer.php'); ?>

  </body>
</html>


    