<div class="full-width navigation area">
  <div class="row">
   <div class="large-12 columns">
    <div class="top-bar">
      <div class="row">
        <div class="top-bar-left">
          <ul class="dropdown menu" data-dropdown-menu>
            <li class="menu-text"><a href="index">Example Artist Site</a></li>
            <li><a href="about">About</a></li>
            <li><a href="events">Events</a></li>
            <li><a href="store">Store</a></li>
          </ul> <!-- end dropdown menu -->
        </div> <!-- end top-bar-left -->
      </div> <!-- end row -->
    </div> <!-- end top-bar -->
  </div> <!-- end large-12 columns -->
 </div> <!-- end row-->
</div> <!-- end navigation area -->