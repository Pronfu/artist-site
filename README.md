# artist-site
Website template for a band / artist. Edit the site to fit your needs.

![Screenshot of website](https://i.imgur.com/sZzP9QS.png)

### LICENSE

* This template is under zlib license, which means this is provided 'as-is' and the authors are not liable for any damages that may arise. Please see the [full license](https://en.wikipedia.org/wiki/Zlib_License) for more details.

### Live Demo
[https://projects.gregoryhammond.ca/artist-site/](https://projects.gregoryhammond.ca/artist-site/)

### FAQ / CONTACT / TROUBLESHOOT

> Do I need a website to get this to work?

* No, you don't. You can run this locally on your computer, or have this just for practice (Find out how to do it thanks to [MakeTechEasier](https://www.maketecheasier.com/setup-local-web-server-all-platforms/) ), but if you wish to show your friends, family and coworkers what you have worked on then you will need to buy a domain and hosting.

